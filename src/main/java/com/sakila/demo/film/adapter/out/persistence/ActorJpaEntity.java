package com.sakila.demo.film.adapter.out.persistence;


import com.sakila.demo.common.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Comment;

import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@Table(
        name = "actor",
        indexes = @Index(name = "idx_actor_last_name", columnList = "last_name"))
public class ActorJpaEntity extends BaseEntity {

    @Id
    @Comment("고유 아이디 번호")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Comment("이름")
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Comment("이름 성")
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Comment("관련 영화")
    @ManyToMany(mappedBy = "actors")
    Set<FilmJpaEntity> films;
}
