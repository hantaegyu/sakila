package com.sakila.demo.film.adapter.out.persistence;

public enum Rating {
    G,
    PG,
    PG_13,
    R,
    NC_17
}