package com.sakila.demo.film.adapter.out.persistence;


import com.sakila.demo.film.domain.Category;
import com.sakila.demo.film.domain.Category.CategoryId;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {
    Category mapToDomainEntity(CategoryJpaEntity JpaEntity) {
        return Category.builder()
                .id(CategoryId.of(JpaEntity.getId()))
                .name(JpaEntity.getName())
                .build();
    }
}
