package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Comment;


@Getter
@Setter
@Builder
@Entity
@Table(name = "language")
public class LanguageJpaEntity extends BaseEntity {

    @Id
    @Comment("고유 아이디 번호")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Comment("나라 이름")
    @Column(name = "name", nullable = false)
    private String name;
}