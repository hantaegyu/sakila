package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Comment;

import java.util.Set;


@Getter
@Setter
@Entity
@Builder
@Table(name = "category")
public class CategoryJpaEntity extends BaseEntity {

    @Id
    @Comment("고유 아이디 번호")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Comment("카테고리 이름")
    @Column(name = "name", nullable = false)
    private String name;

    @Comment("관련 영화")
    @ManyToMany(mappedBy = "categories")
    Set<FilmJpaEntity> films;
}