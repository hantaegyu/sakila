package com.sakila.demo.film.adapter.in.api;

import com.sakila.demo.common.annotation.WebAdapter;
import com.sakila.demo.film.application.port.in.RegisterCategoryCommand;
import com.sakila.demo.film.application.port.in.RegisterCategoryUseCase;
import com.sakila.demo.film.domain.Category.CategoryId;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@WebAdapter
@RequiredArgsConstructor
@RestController
@RequestMapping("/film/category")
public class RegisterCategoryController {

    private final RegisterCategoryUseCase registerCategoryUseCase;

    @PostMapping
    public ResponseEntity<RegisterResponseDto> register(
            @RequestParam String name
    ) {
        RegisterCategoryCommand command = new RegisterCategoryCommand(name);

        CategoryId categoryId = registerCategoryUseCase.register(command);

        RegisterResponseDto responseDto = RegisterResponseDto.builder()
                .id(categoryId.getValue())
                .name(name)
                .build();

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(responseDto);
    }

    @Getter
    @Builder
    private static class RegisterResponseDto {
        private Long id;
        private String name;
    }
}