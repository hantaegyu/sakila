package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.entity.BaseEntity;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Comment;

import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@Builder
@Entity
@Table(
        name = "film",
        indexes = {
                @Index(name = "idx_title", columnList = "title"),
                @Index(name = "idx_fk_language_id", columnList = "language_id"),
                @Index(name = "idx_fk_original_language_id", columnList = "original_language_id"),})
public class FilmJpaEntity extends BaseEntity {

    @Id
    @Comment("고유 아이디 번호")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Comment("제목")
    @Column(name = "title", nullable = false)
    private String title;

    @Comment("설명")
    @Column(name = "description")
    private String description;

    @Comment("출시 연도")
    @Column(name = "release_year")
    private Short releaseYear;

    @Comment("원본 영화 언어")
    @Column(name = "rental_duration", nullable = false)
    private Short rentalDuration;

    @Comment("임대료")
    @Column(name = "rental_rate", nullable = false, precision = 4, scale = 2)
    private BigDecimal rentalRate;

    @Comment("임대료")
    @Column(name = "length", precision = 4, scale = 2)
    private Integer length;

    @Comment("대여 손상 청구 금액")
    @Column(name = "replacement_cost", nullable = false, precision = 5, scale = 2)
    private BigDecimal replacementCost;

    @Comment("평가")
    @Enumerated(EnumType.STRING)
    @Column(name = "rating")
    private Rating rating;

    @ManyToOne
    @Comment("더빙 언어")
    @JoinColumn(name = "language_id", nullable = false)
    private LanguageJpaEntity language;

    @ManyToOne
    @Comment("원본 영화 언어")
    @JoinColumn(name = "original_language_id")
    private LanguageJpaEntity originalLanguage;

    @Comment("배우")
    @ManyToMany
    @JoinTable(
            name = "film_actor",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "actor_id"))
    Set<ActorJpaEntity> actors;

    @Comment("카테고리")
    @ManyToMany
    @JoinTable(
            name = "film_category",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    Set<CategoryJpaEntity> categories;

    @ElementCollection(targetClass = SpecialFeature.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(
            name = "film_special_features",
            joinColumns = @JoinColumn(name = "id"))
    @Column(name = "special_feature")
    private Set<SpecialFeature> specialFeatures;
}