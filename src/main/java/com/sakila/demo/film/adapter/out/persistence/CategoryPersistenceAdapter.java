package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.annotation.PersistenceAdapter;
import com.sakila.demo.film.application.port.out.RegisterCategoryPort;
import com.sakila.demo.film.domain.Category;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@PersistenceAdapter
public class CategoryPersistenceAdapter
        implements RegisterCategoryPort {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    @Override
    public Category register(String name) {
        CategoryJpaEntity category = categoryRepository.save(
                CategoryJpaEntity.builder()
                        .name(name)
                        .build());

        return categoryMapper.mapToDomainEntity(category);
    }
}