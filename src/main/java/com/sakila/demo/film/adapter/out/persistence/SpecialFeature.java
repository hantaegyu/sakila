package com.sakila.demo.film.adapter.out.persistence;

public enum SpecialFeature {
    TRAILERS,
    COMMENTARIES,
    DELETED_SCENES,
    BEHIND_THE_SCENES
}