package com.sakila.demo.film.domain;

import lombok.*;

import java.util.Set;


@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Category {

    private CategoryId id;

    @Getter
    private String name;

    @Getter
    private Set<Film> films;

    public CategoryId getId(){
        return this.id;
    }

    public static Category withoutId(String name, Set<Film> films) {
        return new Category(null, name, films);
    }

    public static Category withId(CategoryId id, String name, Set<Film> films) {
        return new Category(id, name, films);
    }

    @Value(staticConstructor = "of")
    public static class CategoryId {
        private final Long value;
    }
}