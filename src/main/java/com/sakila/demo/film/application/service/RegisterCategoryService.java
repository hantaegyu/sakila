package com.sakila.demo.film.application.service;

import com.sakila.demo.common.annotation.UseCase;
import com.sakila.demo.film.application.port.in.RegisterCategoryCommand;
import com.sakila.demo.film.application.port.in.RegisterCategoryUseCase;
import com.sakila.demo.film.application.port.out.RegisterCategoryPort;
import com.sakila.demo.film.domain.Category.CategoryId;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@UseCase
@Transactional
public class RegisterCategoryService implements RegisterCategoryUseCase {

    private final RegisterCategoryPort registerCategoryPort;

    @Override
    public CategoryId register(RegisterCategoryCommand command) {
        return registerCategoryPort.register(command.name())
                .getId();
    }
}
