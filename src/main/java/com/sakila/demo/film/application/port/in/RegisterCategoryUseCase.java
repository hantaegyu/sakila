package com.sakila.demo.film.application.port.in;

import com.sakila.demo.film.domain.Category.CategoryId;

public interface RegisterCategoryUseCase {
    CategoryId register(RegisterCategoryCommand command);
}