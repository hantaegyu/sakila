package com.sakila.demo.film.application.port.in;

import lombok.NonNull;
public record RegisterCategoryCommand(
        @NonNull String name
) {}