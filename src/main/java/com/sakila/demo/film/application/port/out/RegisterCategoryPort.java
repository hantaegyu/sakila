package com.sakila.demo.film.application.port.out;

import com.sakila.demo.film.domain.Category;

public interface RegisterCategoryPort {
    Category register(String name);
}