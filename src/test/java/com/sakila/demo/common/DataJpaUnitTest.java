package com.sakila.demo.common;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@DataJpaTest(properties = {
        "spring.datasource.url=jdbc:h2:mem:testdb",
        "spring.datasource.driverClassName=org.h2.Driver",
        "spring.datasource.username=local_user",
        "spring.datasource.password=local_password",
        "spring.jpa.hibernate.ddl-auto=create-drop", // 데이터베이스 자동으로 업데이트
        "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect"
})
public @interface DataJpaUnitTest {
}