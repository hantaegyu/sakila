package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.DataJpaUnitTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("JPA | EntityTest | Language")
@DataJpaUnitTest
public class LanguageJpaEntityTest {

    @Autowired
    private LanguageRepository languageRepository;

    @Test
    @DisplayName("생성 | Language JPA Entity")
    public void createLanguageTest() {

        LanguageJpaEntity saved = languageRepository.save(
                LanguageJpaEntity.builder()
                        .name("English")
                        .build());

        LanguageJpaEntity found = languageRepository.findById(saved.getId())
                .orElseThrow();

        assertEntityFields(saved, found);
    }

    private void assertEntityFields(LanguageJpaEntity actual, LanguageJpaEntity predict) {
        assertEquals(actual.getId(), predict.getId());
        assertEquals(actual.getName(), actual.getName());
        assertEquals(actual.getLastUpdate(), actual.getLastUpdate());
    }
}