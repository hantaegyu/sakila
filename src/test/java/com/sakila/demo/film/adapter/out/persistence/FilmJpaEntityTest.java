package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.DataJpaUnitTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("JPA | EntityTest | Film")
@DataJpaUnitTest
public class FilmJpaEntityTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @Test
    @DisplayName("생성 | Film JPA Entity")
    public void createFilmTest() {

        LanguageJpaEntity savedLanguage = languageRepository
                .save(LanguageJpaEntity.builder().name("English").build());

        FilmJpaEntity saved = filmRepository.save(
                FilmJpaEntity.builder()
                        .title("ACE GOLDFINGER")
                        .description("A Astounding Epistle of a Database Administrator And a Explorer who must Find a Car in Ancient China")
                        .releaseYear(Short.valueOf("2006"))
                        .language(savedLanguage)
                        .originalLanguage(savedLanguage)
                        .rentalDuration(Short.valueOf("6"))
                        .rentalRate(BigDecimal.valueOf(2.99))
                        .length(130)
                        .replacementCost(BigDecimal.valueOf(22.99))
                        .rating(Rating.valueOf("G"))
                        .specialFeatures(Set.of(SpecialFeature.DELETED_SCENES))
                        .build());

        FilmJpaEntity found = filmRepository.findById(saved.getId())
                .orElseThrow();

        assertEntityFields(saved, found);
    }

    private void assertEntityFields(FilmJpaEntity actual, FilmJpaEntity predict) {
        assertEquals(actual.getTitle(), predict.getTitle());
        assertEquals(actual.getDescription(), predict.getDescription());
        assertEquals(actual.getReleaseYear(), predict.getReleaseYear());
        assertEquals(actual.getLanguage(), predict.getLanguage());
        assertEquals(actual.getOriginalLanguage(), predict.getOriginalLanguage());
        assertEquals(actual.getRentalDuration(), predict.getRentalDuration());
        assertEquals(actual.getRentalRate(), predict.getRentalRate());
        assertEquals(actual.getLength(), predict.getLength());
        assertEquals(actual.getReplacementCost(), predict.getReplacementCost());
        assertEquals(actual.getRating(), predict.getRating());
        assertEquals(actual.getSpecialFeatures(), predict.getSpecialFeatures());
    }
}