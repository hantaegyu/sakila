package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.DataJpaUnitTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("JPA | EntityTest | Actor")
@DataJpaUnitTest
public class ActorJpaEntityTest {

    @Autowired
    private ActorRepository actorRepository;

    @Test
    @DisplayName("생성 | Actor JPA Entity")
    public void createActor()  {

        ActorJpaEntity saved = actorRepository.save(
                ActorJpaEntity.builder()
                        .firstName("태규")
                        .lastName("한")
                        .build());

        ActorJpaEntity found = actorRepository.findById(saved.getId())
                .orElseThrow();

        assertEntityFields(saved, found);
    }

    private void assertEntityFields(ActorJpaEntity actual, ActorJpaEntity predict) {
        assertEquals(actual.getId(), predict.getId());
        assertEquals(actual.getFirstName(), actual.getFirstName());
        assertEquals(actual.getLastName(), actual.getLastName());
        assertEquals(actual.getLastUpdate(), actual.getLastUpdate());
    }
}