package com.sakila.demo.film.adapter.in.api;


import com.sakila.demo.film.application.port.in.RegisterCategoryCommand;
import com.sakila.demo.film.application.port.in.RegisterCategoryUseCase;
import com.sakila.demo.film.domain.Category;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Web API | ControllerTest | Category")
@WebMvcTest(controllers = RegisterCategoryController.class)
class RegisterCategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    RegisterCategoryUseCase registerCategoryUseCase;

    @Test
    @DisplayName("성공 | CREATE | Category")
    public void testSuccessCreateCategory() throws Exception {

        // given
        RegisterCategoryCommand command = new RegisterCategoryCommand("Action");
        Category.CategoryId expectedCategoryId = Category.CategoryId.of(1L);

        // when
        when(registerCategoryUseCase.register(command))
                .thenReturn(expectedCategoryId);

        mockMvc.perform(post("/film/category")
                        .param("name", "Action")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json("{\"id\": 1, \"name\":  \"Action\"}"));
    }

    @Test
    @DisplayName("실패 | CREATE | Category | name 파라미터 없음")
    public void testFailCreateCategory_NoParameters() throws Exception {

        // given
        RegisterCategoryCommand command = new RegisterCategoryCommand("Action");
        Category.CategoryId expectedCategoryId = Category.CategoryId.of(1L);

        // when
        when(registerCategoryUseCase.register(command))
                .thenReturn(expectedCategoryId);

        mockMvc.perform(post("/film/category")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}