package com.sakila.demo.film.adapter.out.persistence;

import com.sakila.demo.common.DataJpaUnitTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("JPA | EntityTest | Category")
@DataJpaUnitTest
public class CategoryJpaEntityTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    @DisplayName("생성 | Category JPA Entity")
    public void createCategoryTest() {

        CategoryJpaEntity saved = categoryRepository.save(
                CategoryJpaEntity.builder()
                        .name("Animation")
                        .build());

        CategoryJpaEntity found = categoryRepository.findById(saved.getId())
                .orElseThrow();

        assertEntityFields(saved, found);
    }

    private void assertEntityFields(CategoryJpaEntity actual, CategoryJpaEntity predict) {
        assertEquals(actual.getId(), predict.getId());
        assertEquals(actual.getName(), actual.getName());
        assertEquals(actual.getLastUpdate(), actual.getLastUpdate());
    }
}